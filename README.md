# NeMALA Tools #

### Don't touch the core barehanded!

NeMALA Tools is a toolbox that helps manage and debug NeMALA nodes. The tools are a group of standalone 
micro-programs, 
encapsulated by a facade executable. The tools enable node control - synchronize, terminate, pause-resume; recording and playback; data aggregation and visualization. 

### Docker Image Available at [Docker Hub](https://hub.docker.com/repository/docker/nemala/tools)

## Contents ##
* [Install Guide](doc/start.md)
* [Components](doc/components.md)

## Dependencies ##
| Dependency                        | Minimal Version   | License
| ------------                      | ---------------   | -------
| [NeMALA Core](https://gitlab.com/nemala/core) | 1.0   | [NeMALA IIT](LICENSE)
| [Boost](https://www.boost.org/)   | 1.55.0            | [Boost Software License](https://www.boost.org/users/license.html)
| [PyZMQ](http://zeromq.org/bindings:python/) | 14.5.0  | [BSD 3-Clause Revised License](https://github.com/zeromq/pyzmq/blob/master/COPYING.BSD)
| [matplotlib](https://matplotlib.org/) | 2.0.2         | [matplotlib  2.0.2 License](https://matplotlib.org/users/license.html)

## Contributors ##
### [Leiran Sobih](http://linkedin.com/in/sobih-leiran-05a912188) & [Gal Zamir](http://www.linkedin.com/in/galzamir/), Winter 2015-6
* Node Manager
* Logger
### [Uri Livne](https://www.linkedin.com/in/uri-livne-72469395/), Spring 2016
* Logger
* Player
* Analyzer
* Visualizer
