# Install Guide

## Docker Image Available at [Docker Hub](https://hub.docker.com/repository/docker/nemala/tools)
or
## Install from source
1. Install NeMALA Core. Follow the instructions [here](https://gitlab.com/nemala/core).
1. Install additional python requirements:
    ```console
    user@hostname:~/$ pip install path_to_build_folder/Requirements.txt
    ```
   
1. Install NeMALA Tools.
    1. Run [CMake](https://cmake.org/)
    1. Set the CMake *source* folder to the NeMALA tools *src* folder  and the CMake *build* folder to a separate 
    appropriate folder.
 	1. Run the CMake *Configure* command.
 	1. Select the desired toolchain and makefile generators.
 	1. Fill in the include and library paths for your NeMALA Core installation, including the NeMALA library location
 	 if CMake could'nt find it automatically.
 	1. Run *Configure* again until all variables are filled and no errors appear.
 	    Make sure the install prefix and build type are to your liking.
 	1. Run the CMake *Generate* command.
 	   At this point, the chosen *build* folder has all the make files needed for installing the tools.
    1. Go to the build folder and run the command:
        ```console
        user@hostname:/path_to_build_folder$ make; make install
        ```
