# Components
* [Tools Facade](#facade)
* [Node Manager](#manager)
* [Proxy Launcher](#proxy)
* [Logger](#logger)
* [Player](#player)
* [Analyzer](#analyzer)
* [Visualizer](#visualizer)
## <a name="facade"></a>Tools Facade
Once [installed](start.md), all the NeMALA tools can be found at
```console
user@hostname:/path_to_NeMALA_install_folder/bin/NeMALA_Tools/$
```
The bin directory holds a single python script, nemala.py, which acts as a facade to all other tools.
running
```console
user@hostname:/path_to_NeMALA_install_folder/bin/$ nemala.py
```
results in a list of available commands.

For command specific help, run:
```console
user@hostname:/path_to_NeMALA_install_folder/bin/$ nemala.py command_name
```
where *command_name* is either terminate, pause, resume, timesync, log, replay, analyze, visualize or proxy.

## <a name="manager"></a>Node Manager
The Node Manager sends *service messages* that all nodes can handle, on a reserved *service topic* which 
all nodes subscribe to automatically using their built-in [Service Handler](https://gitlab.com/nemala/core).

The node manager supports the following commands:
* Terminate:

    Order the receiving node to finish whatever it is doing, and stop receiving incoming messages.
    Terminating a node causes its [dispatcher](https://gitlab.com/nemala/core) to
    cleanly exit and shut down the execution node.

* Pause:

    Order the receiving nodes to mute one, some or all of its [Publishers](https://gitlab.com/nemala/core).
    The node continues to process incoming messages, only the muted publishers pause their output.

* Resume:
    
    Order the receiving nodes to resume publication of one, some or all (muted) topics.
    A Publisher previously paused gets un-muted if its topic is on the resume list.

* Synchronize Timestamps:
    
    Orders the receiving nodes to synchronize the timestamps they stamp on their messages.
 
## <a name="proxy"></a>Proxy Launcher
The Proxy Launcher tool takes a proxy name and configuration json file as input, and sets up a [proxy](https://gitlab.com/nemala/core).
The configuration file has to have an entry named *"proxies"*, and proxies entry has to have the 
the endpoints *"publishers"*, *"subscribers"*, and *"logger"* as its children.
For example consider the following proxy.json file:
```json
{
	"proxies" :
	{
		"pipe" :
		{
			"publishers" : "ipc:///tmp/pb",
			"subscribers" : "ipc:///tmp/pf",
			"logger" : "ipc:///tmp/pl"
		},
		"loop-back" :
		{
			"publishers" : "tcp://127.0.0.1:5555",
			"subscribers" : "tcp://127.0.0.1:5554",
			"logger" : "tcp://127.0.0.1:5553"
		},
		"network" :
		{
			"publishers" : "tcp://192.168.1.1:5545",
			"subscribers" : "tcp://192.168.1.1:5544",
			"logger" : "tcp://192.168.1.1:5543"
		}
	}
}
```
Assuming the host's ip address is 192.168.1.1 and that the ports 5543-5 are available and the user is authorized to 
open them, running the command 
```console
nemala.py proxy network proxy.json
```
will run a NeMALA proxy with the provided publishers, subscribers, and logger endpoints.
Publishers, Dispatchers, and Loggers require these endpoints to publish, subscribe, and log respectively.
 
To terminate the proxy, either kill the proxy process or interrupt (CTRL^C) the nemala.py facade.
								
## <a name="logger"></a>Logger
The Logger attaches to a list of proxies and logs all messages going through those 
proxies to a [SQLite](https://www.sqlite.org/) database.
									
## <a name="player"></a>Player
The Player tool replays a log created by the [Logger](#logger), excluding the node manager's service topic.
The player accepts three command line arguments:
* A database file created by the logger.
* A time multiplier *x*- every second in the logged run is translated to *1/x* seconds when replayed.
if *x=0* then all messages will be replayed with no delay between them.
* A comma-separated-values (csv) file in which each line begins with a proxy endpoint to which the 
player publishes to, followed by the topics which will be published to this endpoint.
If the csv contains only one endpoint and no topics, then all topics (except the Node Manager's service topic) get published to that endpoint.

## <a name="analyzer"></a>Analyzer
The analyzer bundles numeric data from different topics into one topic.
									
Since not all messages from different topics arrive at the same time, the analyzer accumulates the data received and publishes once a threshold of user defined minimum samples is accumulated in all subscribed topics.

The analyzer lets the user define what to do with the data being accumulated.
For example,
once all message fields pass the minimum threshold since the last time the analyzer published,
only the most recent data or the average of all data 
accumulated is published, depending on how the Analyzer is configured.

The analyzer is a dispatcher with a single publisher and as many topic handlers as
there are topics the analyzer is interested in.
Each incoming message gets handled by its handler, 
which iterates through its *rename* fields and accumulates their values with the values in the *name* 
fields found in the incoming message.
Once all accumulators are saturated, the analyzer publishes to its topic.

## <a name="visualizer"></a>Visualizer
The visualizer plots numeric data from a single topic published to a dedicated proxy, to which only that topic is published.
The visualizer can plot up to 7 fields from the same topic in *Time-Dependent* method, or two fields in *X-Y* 
method.
