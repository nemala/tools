/*
 * ProxyLauncher.cpp
 *
 *  Created on: Jul 9, 2015
 *      Author: dave
 */

#include <boost/thread/thread.hpp>
#include <boost/asio.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <NeMALA/Proxy.h>
#include <iostream>

void signalHandle(const boost::system::error_code& error, int signal_number)
{
	// A signal occurred.
	if (error)
	{
		std::cerr << "Proxy encountered an error, errno: " << error.value() << ", message: " << error.message() << std::endl;
	}
	else
	{
		std::cout<< "Proxy received signal: " << signal_number << " ,shutting down." << std::endl;
	}
}

int main (int argc, char *argv[])
{
	boost::asio::io_service io_service;
	boost::asio::signal_set signals(io_service, SIGINT, SIGTERM);
	boost::property_tree::ptree pt;
	bool bFailed(false);
	NeMALA::Proxy proxy;
	std::string strProxyName;

	if ( 3 != argc)
	{
	    std::cout << "Usage: " << std::endl;
	    std::cout << argv[0] << " proxy_name config.json" << std::endl;
	    std::cout << "See documentation." << std::endl;
		bFailed = true;
	}
	else
	{
		strProxyName = "proxies.";
		strProxyName += argv[1];
	}

	if(!bFailed)
	{
		// Check input
		try
		{
			boost::property_tree::read_json(argv[2],pt);
		}
		catch (std::exception& e)
		{
			std::cerr << "Error in proxy input: " << e.what() << std::endl;
			bFailed = true;
		}
	}


	if(!bFailed)
	{
		proxy.Init((pt.get<std::string>(strProxyName + ".publishers")).c_str(),
				(pt.get<std::string>(strProxyName + ".subscribers")).c_str(),
				(pt.get<std::string>(strProxyName + ".logger")).c_str());
	}

	if(!bFailed)
	{
		// Run as thread
	    boost::thread proxThread(boost::ref(proxy));

	    signals.async_wait(signalHandle);
	    io_service.run();

	    // Clean all resources.
	    proxy.ShutDown();
	    proxThread.interrupt();
	    proxThread.join(); // wait for thread to close down
	}

    return 0;
}




