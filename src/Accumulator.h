/*
 * Accumulator.h
 *
 *  Created on: May 29, 2017
 *      Author: dave
 */

#ifndef _ACCUMULATOR_H_
#define _ACCUMULATOR_H_

namespace NeMALA_Tools
{

/*
 * The Accumulator accumulates values
*/
class Accumulator
{
public:

	Accumulator(unsigned int unThreshold = 1):m_unThreshold(unThreshold),m_unNumOfSamples(0){}
	virtual ~Accumulator (){}

	/**
	 * Set the minimal amount of samples required for saturation.
	 * @param unThreshold the minimal sample amount
	 */
	void SetThreshold (unsigned int unThreshold){m_unThreshold = unThreshold;}

	/**
	 * Get the minimal amount of samples required for saturation.
	 * @return the minimal sample amount
	 */
	unsigned int GetThreshold(){return m_unThreshold;}

	/**
	 * Accumulate a value
	 * @param dVal the value to accumulate
	 * @return true if saturated due to this accumulation.
	 */
	bool Accumulate(double dVal);

	/*
	 * Returns true if the accumulator accumulated at least the threshold amount of samples.
	 */
	bool IsSaturated(){return (m_unNumOfSamples >= m_unThreshold);}

	/*
	 * Return the current accumulated value and reset the accumulator.
	 */
	 double Dump();

protected:

	 /**
	  * Add a sample to the accumulator.
	  * @param dVal value to be added.
	  */
	 virtual void AddSample(double dVal)=0;

	 /**
	  * Reset the accumulator
	  */
	 virtual void Reset()=0;

	 /**
	  * Get the accumulated value
	  */
	 virtual double Get()=0;

private:

	 unsigned int m_unThreshold;
	 unsigned int m_unNumOfSamples;

};

} // namespace NeMALA_Tools

#endif // _ACCUMULATOR_H_
