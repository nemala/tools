#!/usr/bin/env python3
import sys
from NeMALA_Tools import NodeManager as nm
from NeMALA_Tools import Logger as lgr
from NeMALA_Tools import Replayer as rplr
from NeMALA_Tools import Visualizer as vslr
from NeMALA_Tools import AnalyzerLocation as anlyzr
from NeMALA_Tools import ProxyLocation as prxy
import subprocess

"""
###################################################################################
class NeMALA_Tools - a facade class for NeMALA tools.
###################################################################################
"""
class NeMALA_Tools:
    def __init__(self):
        self._result = 1
        self._manager = nm.NodeManager()
        
    def execute(self,argv):
        if (2 > len(argv)):
            self.usage(argv[0])
        else:
            op = argv[1]
            if self._manager.checkOperation(op):
                self._result = self._manager.execute(argv)
            elif ('log' == op):
                self._result = lgr.log([argv[0] + ' ' + op] + argv[2:])
            elif ('replay' == op):
                self._result = rplr.replay([argv[0] + ' ' + op] + argv[2:])
            elif ('analyze' == op):
                self._result = subprocess.call([anlyzr] + argv[2:])
            elif ('visualize' == op):
                self._result = vslr.run([argv[0] + ' ' + op] + argv[2:])
            elif ('proxy' == op):
                try:
                    self._result = subprocess.call([prxy] + argv[2:])
                except KeyboardInterrupt as err:
                    pass
            else:
                self.usage(argv[0])
        
        return self._result
                     
    def usage(self, executableName):
        print("usage: " + executableName + " <command> ...")
        print("command: Either terminate, pause, resume, timesync, log, replay, analyze, visualize or proxy")
        print("for command specific help type " + executableName + " <command>")

    
###################################################################################
# Run the main function 
###################################################################################
if __name__ == '__main__':
    tools = NeMALA_Tools()
    sys.exit(tools.execute(sys.argv))
    