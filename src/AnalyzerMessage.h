/*
 * AnalyzerMessage.h
 *
 *  Created on: Jun 1, 2017
 *      Author: dave
 */

#ifndef _ANALYZER_MESSAGE_H_
#define _ANALYZER_MESSAGE_H_

#include <NeMALA/MessagePropertyTree.h>
#include <map>


namespace NeMALA_Tools
{

/**
  * class AnlayzerMessage
  * The Analyzer Message has no members other than the underlying
  * property tree, and is populated ad-hoc by the analyzer to be shipped off with no
  * constant structure.
  */

class AnalyzerMessage : public NeMALA::MessagePropertyTree
{

public:

	/**
	 * Constructor
	 *  @param mFields A <name,value> map containing the message fields.
	 */
	AnalyzerMessage(std::map<std::string, double> mFields);


	/*
	 * Constructor
	 * Build a message from a property tree.
	 * Subscribers should use this function.
	 * @param pt: The property tree received from a publisher which translated to message.
	 */
	AnalyzerMessage(NeMALA::Proptree pt);

	~AnalyzerMessage(){}

private:

	std::map<std::string, double> m_mFeilds;

	void BuildMessage(); //called from constructor
};

}// namespace NeMALA_Tools


#endif // _ANALYZER_MESSAGE_H_
