/*
 * MessagePropertyTree.h
 *
 *  Created on: May 23, 2016
 *      Author: dave
 */

#include "Analyzer.h"
#include "AnalyzerHandler.h"
#include "AccumulatorMostRecent.h"
#include <iostream>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>

int main(int argc, char** argv)
{
	boost::property_tree::ptree pt;
	NeMALA_Tools::Analyzer* pAnalyzer(NULL);
	bool bFailed(false);
	int nHandlers(0);
	int nAccumulators(0);
	std::map<int ,NeMALA_Tools::AnalyzerHandler*> mHandlers;
	std::map<std::string ,NeMALA_Tools::Accumulator*> mAccumulators;


	if ( 2 == argc)
	{
		// Check input
		try
		{
			boost::property_tree::read_json(argv[1],pt);
		}
		catch (std::exception& e)
		{
			std::cerr << "Error in Analyzer input: " << e.what() << std::endl;
			bFailed = true;
		}

		if (!bFailed)
		{
			// set up analyzer
			try
			{
				pAnalyzer = new NeMALA_Tools::Analyzer(pt.get<unsigned int>("node_id"),
						pt.get<std::string>("subscribe_to_system_endpoint"),
						pt.get<std::string>("publish_endpoint"),
						pt.get<unsigned int>("publish_topic"));
			}
			catch (std::exception& e)
			{
				std::cerr << "Failed to initialize Analyzer: " << e.what() << std::endl;
				bFailed = true;
			}
		}

		if (!bFailed)
		{
			pAnalyzer->PrintVersion();

			// For each field of interest,
			BOOST_FOREACH(boost::property_tree::ptree::value_type& interested, pt.get_child("interested_in"))
			{
				std::map<std::string,std::string> smRenamings;
				// get the topic and create a handler for it.
				BOOST_FOREACH(boost::property_tree::ptree::value_type& ptFields, interested.second.get_child("fields"))
				{
					NeMALA_Tools::Accumulator* pAccumulator;

					smRenamings[ptFields.second.get<std::string>("name")] = ptFields.second.get<std::string>("rename");
					pAccumulator = new NeMALA_Tools::AccumulatorMostRecent();
					mAccumulators[ptFields.second.get<std::string>("rename")] = pAccumulator;

					pAnalyzer->AddAccumulator(ptFields.second.get<std::string>("rename"),pAccumulator);
				}

				mHandlers[nHandlers] = new NeMALA_Tools::AnalyzerHandler(pAnalyzer, smRenamings);
				pAnalyzer->AddHandler(interested.second.get<unsigned int>("topic"), mHandlers[nHandlers++]);
			}

			// Subscribe
			BOOST_FOREACH(boost::property_tree::ptree::value_type& v, pt.get_child("subscribe_to_endpoints"))
			{
				pAnalyzer->Subscribe((v.second.get_value<std::string>()).c_str());
			}

			try
			{
				pAnalyzer->Dispatch();
			}
			catch (std::exception& e)
			{
				std::cerr << "Error in Analyzer: " << e.what() << std::endl;
			}

			std::cout << "Shutting down " << argv[0] << std::endl;

			delete pAnalyzer;
			for (std::map<int ,NeMALA_Tools::AnalyzerHandler*>::iterator it=mHandlers.begin(); it!=mHandlers.end(); ++it)
			{
				delete it->second;
			}
			for (std::map<std::string ,NeMALA_Tools::Accumulator*>::iterator it=mAccumulators.begin(); it!=mAccumulators.end(); ++it)
			{
				delete it->second;
			}
		}
	}
	else //case of wrong input
	{
	    std::cout << "Usage: " << std::endl;
	    std::cout << argv[0] << " analyzer.config" << std::endl;
	    std::cout << "See documentation." << std::endl;
	}
	return 0;
}
