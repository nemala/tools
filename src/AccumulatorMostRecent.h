/*
 * AccumulatorMostRecent.h
 *
 *  Created on: May 29, 2017
 *      Author: dave
 */

#ifndef _ACCUMULATOR_MOST_RECENT_H_
#define _ACCUMULATOR_MOST_RECENT_H_

#include "Accumulator.h"

namespace NeMALA_Tools
{

class AccumulatorMostRecent: public Accumulator
{
public:
	AccumulatorMostRecent():Accumulator(),m_dVAl(0){}
	virtual ~AccumulatorMostRecent(){}

private:
	double m_dVAl;

	 /**
	  * Add a sample to the accumulator.
	  * @param dVal value to be added.
	  */
	 virtual void AddSample(double dVal){m_dVAl = dVal;}

	 /**
	  * Reset the accumulator
	  */
	 virtual void Reset(){}

	 /**
	  * Get the accumulated value
	  */
	 virtual double Get(){return m_dVAl;}

};

} //namespace NeMALA_Tools

#endif /* _ACCUMULATOR_MOST_RECENT_H_ */
