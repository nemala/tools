/*
 * Accumulator.cpp
 *
 *  Created on: May 29, 2017
 *      Author: dave
 */

#include "Accumulator.h"

namespace NeMALA_Tools
{


/**
 * Accumulate a value
 * @param dVal the value to accumulate
 * @return true if saturated due to this accumulation.
 */
bool Accumulator::Accumulate(double dVal)
{
	bool bResult(false);
	bool bWasSaturated = IsSaturated();

	AddSample(dVal);
	m_unNumOfSamples++;

	// If became saturated as a result of this operation
	if ((false == bWasSaturated) && IsSaturated())
	{
		bResult = true;
	}

	return bResult;
}

/*
 * Return the current accumulated value and reset the accumulator.
 */
double Accumulator::Dump()
{
	double dResult = Get();
	Reset();
	m_unNumOfSamples = 0;
	return dResult;
}

} //namespace NeMALA_Tools
