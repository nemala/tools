/*
 * AnalyzerHandler.h
 *
 *  Created on: May 29, 2016
 *      Author: dave
 */

#ifndef _ANALYZER_HANDLER_H_
#define _ANALYZER_HANDLER_H_

#include "Analyzer.h"
#include <NeMALA/Handler.h>
#include <map>

namespace NeMALA_Tools
{

class AnalyzerHandler : public NeMALA::Handler
{
public:

	// The constructor expects a smFields map of <name,rename> pairs.
	AnalyzerHandler(NeMALA_Tools::Analyzer* pAnalyzer, std::map<std::string,std::string> smFields):
		m_pAnalyzer(pAnalyzer),m_smFields(smFields){}

	~AnalyzerHandler(){}

	/*
	 * Handles incoming message by calling the analyzer's accumulation procedure.
	 */
	void Handle(NeMALA::Proptree ptMsgBody);

private:
	std::map<std::string,std::string> m_smFields;
	NeMALA_Tools::Analyzer* m_pAnalyzer;
};

} // namespace NeMALA_Tools

#endif /* _ANALYZER_HANDLER_H_ */
