#!/usr/bin/env python3
import zmq
import sqlite3
import sys
import datetime
import time
import os
import queue
from lxml.etree import Element, SubElement, tostring

"""
Defines for Replayer.HandleEndpointsFile function
"""
ALL_TOPICS = -1
"""
Defines for Replayer.convertToTuple function
"""
NAME = 1
TYPE = 2
"""
Defines for Replayer.replaceTimeStamp function
"""
TS_START = '<TimeStamp>'
TS_END = '</TimeStamp>'

"""
###################################################################################
usage function
###################################################################################
"""
def usage(executableName):
    print("usage: " + executableName + " <sqlite database file> <simulation speed> <endpointsFile>")
    print("sql file             Path of SQL file created by logger tool")
    print("simulation speed     Desired multiplication of original time period in SQL file ")
    print("endpointsFile        csv file of endpoints and matching topics - see documentation for details")
    return

"""
###################################################################################
class ReplayerNext - a data structure which holds the data for next message that shuold be sent.
                     used in Replayer.replayLoop method.
###################################################################################
"""
class ReplayerNext:

    def __init__(self, topic,  timestamp, message):
        self.topic = topic
        self.timestamp = timestamp
        self.message = message



"""
###################################################################################
class Replayer - a data structure which simulates a node in the system. stores the node topic,
                 a publisher and a MessageQueue which contains tuples that represent the messages sent by the node.
###################################################################################
"""
class ReplayerPublisher :

    def __init__(self, topic, context, endpoint):
        self.publisher = context.socket(zmq.PUB)
        self.publisher.connect(endpoint)
        time.sleep(1)
        self.messagesQueue = Queue.Queue()



"""
###################################################################################
class Replayer - the main class of the script. stores user given parameters, the DB created from the logger file,
                 performs the replay simulation with it's methods.
###################################################################################
"""
class Replayer :

    def __init__(self, logFileName, simRate, endpointsFile ):

        self.simulationRate = float(simRate)

        self.DB = sqlite3.connect(logFileName)

        print('\nCreating simulated topics...\n')
        self.endpointsTopicsDict = self.HandleEndpointsFile(endpointsFile)#key - endpoint string, value list of matching topics
        
        self.context = zmq.Context()

        self.publishersDict = self.sortAndConvertDB() #initilaize publisherDict key - topic , value ReplayerPublisher

        #get the first message
        self.result = self.getNextMessage()


    """
    ###################################################################################
    Replayer.createReplayDB function
    logFileName - the logger dump filename
    Creates SQLite3 DB according to the queries in the logFile.
    Each Topic has it's own table
    Each table contains messages from it's Topic.
    Returns a Connection instance of the DB .
    ###################################################################################
    """

    def HandleEndpointsFile(self, endpoints):
        
        givenFile = open(endpoints, 'r')
        print("Simulated proxy endpoints and topics:\n")
        endpointTopicsDics = {} #key - endpoint string, value - list of topics publishing to it
        lineCount = 0; #will be used to find mistakes in the given file
        # insert endpoint and matching topics
        for line in givenFile:
            lineSplited = line.split(',')
            #remove \n char
            lineSplited = [word.replace('\n',"") for word in lineSplited ]
            #check the case of only endpoint given (default endpoint for all topics)
            if  len (lineSplited) == 1 or all( not(word.isdigit()) for word in lineSplited) :
                if lineCount == 0 : #case of a single endpoint in file, no topics specified
                    endpointTopicsDics[lineSplited[0]] = ALL_TOPICS
                    print(lineSplited[0] + ' All Topics\n')
                    break
                else :
                    raise ValueError("input file doesn't match expected format")
            else:
                """
                #verfiy that all but the first word are numbers
                for word in lineSplited[1:len(lineSplited)] :
                    if ( not (word.isdigit()) and word != "" and word != '\n' ) :
                        print(word)
                        raise ValueError("input file doesn't match expected format")
                """
                #match topics to endpoints according to input file, ignore spaces after commas and new line
                endpointTopicsDics[lineSplited[0]] = [int(topic) for topic in lineSplited[1:len(lineSplited)]
                                                      if ( topic != "" and topic != '\n' )]
                print(lineSplited[0], endpointTopicsDics[lineSplited[0]] + '\n')
                lineCount += 1
        return  endpointTopicsDics



    """
     ###################################################################################
     Replayer.sortAndConvertDB function
     For each table create a ReplayerPublisher object which contains publisher, messages queue.
     For each  table sort by timestamp, ascending order (earliest timestamp first).
     From each table, convert rows to message format (string) and store it in the appropriate messages queue.
     Returns a dictionary of ReplayerPublisher (key - topic, value - ReplayerPublisher)
     ###################################################################################
     """
    def sortAndConvertDB(self):

        cur = self.DB.cursor()
        # create a list of table names in the db
        cur.execute('select name from sqlite_master where type=\'table\'')
        tables = map(lambda t: t[0], cur.fetchall())
        # print tables

        # initiliaze publishers dictionary
        publishersDict = dict()  # Key - Topic number, Value - the appropriate ReplayerPublisher

        matchingEndpoint = ''
        isDefaultEndpoint = False
        if len(self.endpointsTopicsDict ) == 1 and self.endpointsTopicsDict.values()[0] == ALL_TOPICS:  # case of single endpoint for all topics
            matchingEndpoint = self.endpointsTopicsDict.keys()[0]
            isDefaultEndpoint = True

        # for each table create a queue, sort the table and convert it's rows to messages
        for table in tables:
            topic = int(table.replace('Topic',''))
            if topic == 0 : #doesn't handle service messages
                continue

            #get matching endpoint from self.endpointsTopicsDict
            if   isDefaultEndpoint == False  :
                matchingEndpoint = ''
                for endpoint in self.endpointsTopicsDict.keys() :
                    if topic in self.endpointsTopicsDict[endpoint] :
                        if matchingEndpoint != '' : #verify that each topic matches exactly one endpoint
                            self.DB.close()
                            raise ValueError('a topic matches more than one endpoint - check input file', topic)
                        matchingEndpoint = endpoint

                if matchingEndpoint == '' : #if the topic was not requested by the user - do nothing & continue to next table
                    continue


            publishersDict[topic] = ReplayerPublisher(topic, self.context, matchingEndpoint)

            t = (table,)
            # get the info of the table's columns
            columnInfoQuery = 'pragma table_info(%s)' % t
            columnInfo = cur.execute(columnInfoQuery).fetchall()

            # sort the table
            sort_query = 'select * from %s order by TimeStamp' % t
            sortedDBTable = cur.execute(sort_query)
            # convert to tuple
            for row in sortedDBTable:
                rowTuple = self.convertToTuple(table, row, columnInfo)

                publishersDict[topic].messagesQueue.put(rowTuple)
        cur.close()

        return publishersDict



    """
    ###################################################################################
    Replayer.convertToTupleXML function
    table - table and Topic name
    sqliteRow - row in sqlite format
    columnInfo - description of the table columns as returned from columnInfoQuery
                 columnInfo format - list of 6-tuples ,  one for each column:
                 (column number, column name, data type, NULL option, default value, unknown)
                 Example: [(0, 'Message', 'text', 0, None, 0), (1, 'Sin', 'real', 0, None, 0)]
    Converts the row to a string which represents a message
    Returns a tuple (timestamp, message).
    ###################################################################################
    """


    def convertToTuple(self, table, sqliteRow, columnInfo):

        strSqliteRow = [str(val) for val in sqliteRow]  # for string concatenation afterwards
        topic = table.replace('Topic', '') #numeric string
        length = len(strSqliteRow)

        root = Element('Message')
        headerElement = SubElement(root, 'Header')
        topicElement = SubElement(headerElement, 'Topic') #XML element
        topicElement.text = topic
        timeStElement = SubElement(headerElement, 'TimeStamp')
        timeStElement.text =  sqliteRow[length - 1]
        bodyElement = SubElement(root, 'Body')
        #add the fields of the topic
        for i in range(length - 1):

                fieldElement = SubElement(bodyElement, columnInfo[i][NAME], attrib={'type': columnInfo[i][TYPE]})
                fieldElement.text = strSqliteRow[i]

        timeStamp =  datetime.datetime.strptime(sqliteRow[length - 1], "%Y-%b-%d %H:%M:%S.%f") # datetime format , NOT string!
        xmlString = tostring(root, encoding="utf-8", xml_declaration=True)
        return (timeStamp, xmlString)


    """
    ###################################################################################
    Replayer.replayLoop function
    On every iteration checks self.result which is determined by self.getNextMessage function.
    Calculates time difference between messages according to self.simulationRate
    Publishes the message
    Exits when there are no more messages to publish (self.result is None)
    ###################################################################################
    """

    def replayLoop(self):

        if self.result == None:
            return
        print('Replaying messages...\n')
        oldTimeStamp = self.result.timestamp
        replayStartTime = time.time()  # debug

        while ( self.result ):  # while not all queues are empty
            try:
                if self.simulationRate != 0: #calculate simulation sleep time, then sleep
                    timeDiff = self.result.timestamp - oldTimeStamp  # timeDiff type is timedelta
                    sleepTime = timeDiff.seconds + float(timeDiff.microseconds) / 1000000
                    time.sleep(sleepTime / self.simulationRate)

                #set the currnet time as time stamp
                messageToPublish = self.replaceTimeStamp()

                #publish
                self.publishersDict[self.result.topic].publisher.send(messageToPublish)
                print(messageToPublish)  #for debug

                # print 'Time Between messages: ', time.time() - testStartTime #for debug

                oldTimeStamp = self.result.timestamp
                self.result = self.getNextMessage()
            except KeyboardInterrupt:
                print("Replayer received signal, shutting down")
                break

        print('Total Replay Time: ', time.time() - replayStartTime)
        time.sleep(1)  # end of replayer run - wait to clear buffers

        # close publishers
        for topic in self.publishersDict:
            self.publishersDict[topic].publisher.close()

        # terminate context and close DB
        self.context.term()
        self.DB.close()



    """
    ###################################################################################
    Replayer.getNextMessage function
    Returns a ReplayerNext object or None. None is returned if and only if all queues are empty.
    The returned object is the one with the earliest message in publisherDict
    ###################################################################################
    """
    def getNextMessage(self):

        earlyTimeStamp = datetime.datetime(datetime.MAXYEAR, 1, 1, 0, 0, 0, 0)
        earlyTopic = None  # if remains None after the following loop it indicates that all queues are empty.
        for topic, rp in self.publishersDict.iteritems():
            if not ( rp.messagesQueue.empty() ):
                if rp.messagesQueue.queue[0][0] < earlyTimeStamp:
                    earlyTopic = topic
                    earlyTimeStamp = rp.messagesQueue.queue[0][0]

        if earlyTopic == None:
            return None
        else:
            nextTuple = self.publishersDict[earlyTopic].messagesQueue.get() # pop the first tuple
            next = ReplayerNext(earlyTopic, nextTuple[0], nextTuple[1])
            return next



    """
    ###################################################################################
    Replayer.replaceTimeStamp function
    Returns a message identical to current message in self.result object, with it's
    time stamp changed to current operation system time
    ###################################################################################
    """
    def replaceTimeStamp(self):
        message = self.result.message

        # find start index of old timestamp
        start = message.find(TS_START)
        assert start != -1, 'no timeStamp in message'
        start += len(TS_START)
        # find end index of old timestamp
        end = message.find(TS_END)
        # concatenate message[start-1] + currentTimeStamp*simulationRate + message[end+1]
        messageToReturn = message[:start] + datetime.datetime.now().strftime("%Y-%b-%d %H:%M:%S.%f") + message[end:]
        return messageToReturn

"""
###################################################################################
main function
argv[1] - Path of SQL file created by logger tool
argv[2] - Simulation rate - desired time multiplication of sql file time (float)
argv[3] - Endpoint of proxy front-end to which the replayer publishes
command example ./Replay.py dumpTestFile.sql 50 ipc:///tmp/proxy_in.ipc
###################################################################################
"""
def replay(argv):

    #check input correctness
    if ( len(argv) < 4  or  not(os.path.splitext(argv[1])[1] == ".db")):
        usage(argv[0])
        return 0
    try:
        replayer = Replayer(argv[1], argv[2], argv[3])
    except ValueError as err:
        print(err.args)
    except IOError as err:
        print("I/O error({0}): {1}: {2}".format(err.errno, err.strerror, argv[3]))
    else :
        replayer.replayLoop()
    return 0

"""
###################################################################################
# Run the main function
###################################################################################
"""
if __name__ == '__main__':
    sys.exit(replay(sys.argv))
