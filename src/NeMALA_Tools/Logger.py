#!/usr/bin/env python3
import zmq
import sys
import xml.etree.ElementTree as ET
import sqlite3
import time
import datetime

def usage(executableName):
    print("usage:    " + executableName + " <proxy list> ")
    print("proxy list:    A space seperated list containing logging endpoints of proxies.\n" \
          "                  Must contain at least one proxy log endpoint")
    print("Example:    " + executableName + " ipc:///tmp/proxy_log.ipc ipc:///tmp/sproxy_log.ipc")


def handleTopic (element, topic, timeStamp, cur):
    structure = ""
    values = []
    for child in element:
        # Patch!!! having the timestamp in both header and body in timesync service message messes everything up. 
        timesyncPatch = (('0' == topic) and ('time' == child.tag))
        if child.attrib:
            if child.attrib.values()[0] == "real":
                values.append(float(child.text))
            else:
                if not timesyncPatch:
                    values.append(child.text)
        else:
            child.attrib['type'] = 'text'
            childValues = []
            for value in child.itertext():
                childValues.append(value)
            values.append(str(childValues))
        if not timesyncPatch:
            structure = structure + child.tag + ' ' + child.attrib.values()[0] + ', '
    structure += 'TimeStamp datetime'
    values.append(timeStamp)
    query = 'create table if not exists Topic{0} ({1})'.format(topic, structure)
    cur.execute(query)
    query = 'INSERT INTO Topic{0} values ({1})'.format(topic, str(values)[1:-1])
    #print(query)
    cur.execute(query)


def parser(message, cur):
    #print(message)
    root = ET.fromstring(message)
    topic = root.find("Header/Topic").text
    timeStamp = root.find("Header/TimeStamp").text
    body = root.find("Body")
    handleTopic(body, topic, timeStamp, cur)


###################################################################################
# The main function
###################################################################################

def log(argv):
    if len(argv) < 2 :
        usage(argv[0])
        return  0

    context = zmq.Context()
    subscriber = context.socket(zmq.SUB)
    for endpoint in argv[1:len(argv)] :
        subscriber.connect(endpoint)
        time.sleep(1)
    subscriber.setsockopt(zmq.SUBSCRIBE, b"")
    idCounter = 0
    fileName = 'log_{0}.db'.format(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H:%M'))
    db = sqlite3.connect(fileName)
    cur = db.cursor()
    while True:
        try:
            message = subscriber.recv()
            if message[0] == '<':
                parser(message, cur)
            idCounter += 1
        except KeyboardInterrupt:
            break
    print("Logger received signal, shutting down")
    subscriber.close()
    context.term()
    cur.close()
    db.close()
    print('{} elements logged'.format(idCounter))
    return 0


###################################################################################
# Run the main function
###################################################################################
if __name__ == '__main__':
    sys.exit(log(sys.argv))
