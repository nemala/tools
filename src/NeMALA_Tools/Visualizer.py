#!/usr/bin/env python3

import tkinter as tk
import tkinter.ttk as ttk
import csv
import sys
import threading
import xml.etree.ElementTree as ET
import matplotlib
matplotlib.use("TkAgg")
from matplotlib import pyplot
from matplotlib.font_manager import FontProperties
import zmq
import time
import datetime

from NeMALA_Tools import NodeManager

"""
Define for Visualizer.parser function
"""
TS = 'TimeStamp'

"""
Defines for Visualizer.plotData function
"""
MAX_DEPENDET_VARIABLES = 7 #limited by the length of color list below
COLOURS = ['r','b','g', 'c', 'm', 'y', 'k']

"""

Defines for proxyThread function
"""
BACK_ENDPOINT = 'ipc:///tmp/proxyvis_out.ipc'
FRONT_ENDPOINT = 'ipc:///tmp/proxyvis_in.ipc'

"""
Defines for method Options
"""
TIME_DEPENDET = 1
XY = 2

"""
###################################################################################
usage function
###################################################################################
"""
def usage(executableName):
    print("usage: " + executableName + " <proxy backend> <topic> ")
    print("proxy backend        proxy back end from which to subscribe data")
    print("topic                topic to visualize data from ")
    return


class AxisData:

    def __init__(self, name, firstValue):
        self.axisName = name
        self.axisData = [firstValue]

    def setName(self, name):
        self.axisName = name
    def getName(self):
        return self.axisName

    def getData(self):
        return self.axisData

    def addData(self,value):
        self.axisData.append(value)

    def __len__(self):
        return len(self.axisData)

class DataContainer:

    def __init__(self) : #fieldNum = None will be used in Gui init function.
        self.axesDataLists = []
        self.timeStampList = []
        self.IsInitialized = False
        self.lastTimeStamp =  datetime.datetime(datetime.MINYEAR, 1, 1, 0, 0, 0, 0)
        self.lastRelativetime = 0
        self.dataReady = threading.Event() #flag to avoid plotting while data is being updated.
        self.dataReady.set()
        self.notPlotting = threading.Event() #flag to avoid updating data while plotting. initiliazed to false.
        self.notPlotting.set()
        self.isTimeDependent = False

    def getIsInitiliazed(self):
        return self.IsInitialized

    def getLastTimeStamp(self):
        return self.lastTimeStamp



    def storeData(self, pairsList):
        self.dataReady.clear()
        listLen = len(pairsList)
        for i in range( len(pairsList) ) :

            name = pairsList[i][0]
            value = pairsList[i][1]
            if i == 0 : #case of time stamp data - convert to relative time and store timestamp in seperate list
                tsValue = datetime.datetime.strptime(value, "%Y-%b-%d %H:%M:%S.%f") #convert timestamp string to datetime format
                self.timeStampList.append(tsValue)
                value = self.convertTimeStampToRelativeTime(tsValue)
                name = 'Relative Time'

            if not self.IsInitialized:  # dataContainer empty, first message need to insert data

                self.axesDataLists.append(  AxisData(name, value)  ) #intiliaze a list of dictionaries with paris (fieldName, valuesList)
                if i == listLen - 1 : #last pair of first message proccesed, set initilizaed flag to on
                    self.IsInitialized = True

            else :
                self.notPlotting.wait()
                axisData = next((x for x in self.axesDataLists if x.getName() == name), None)
                axisData.addData(value)
                #self.axesDataLists[i][name].append(value)

        self.dataReady.set()


    def convertTimeStampToRelativeTime(self, timeStamp):

        result = 0
        if  len( self.timeStampList) > 1 : #if not first timestamp
            timeDiff = self.timeStampList[-1] - self.timeStampList[-2]
            result = self.axesDataLists[0].getData()[-1] + timeDiff.seconds + ( float( timeDiff.microseconds ) /  1000000 )

        return result




class MethodOptionsFrame(ttk.LabelFrame) :
    def __init__(self, master):
        ttk.LabelFrame.__init__(self)

        self.methodVar = tk.IntVar()
        self.TimeOption = ttk.Radiobutton(self, text="Time-Dependent", variable=self.methodVar, value=TIME_DEPENDET)
        self.TimeOption.grid(row = 3 , column = 3)
        self.XYOption = ttk.Radiobutton(self, text="X-Y", variable=self.methodVar, value=XY)
        self.XYOption.grid(row= 4, column = 3, sticky = tk.W)


        self.configure(text = 'Plotting Method')



    def disableOptions(self):
        self.TimeOption.configure(state = 'disabled')
        self.XYOption.configure(state = 'disabled')


class Options:

    def __init__(self, displayOption, methodOption):
        self.display = displayOption
        self.method = methodOption

    def getDisplayOption(self):
        return  self.display

    def getMethodOption(self):
        return self.method


class collectDataThread(threading.Thread) :

    def __init__(self, visualizer):
        self.vis = visualizer

        threading.Thread.__init__(self, target=self.vis.collectData)

        self.daemon = True



class proxyThread(threading.Thread):

    def __init__(self, visualizer):
        self.vis = visualizer

        threading.Thread.__init__(self, target=self.vis.runProxy)

        self.daemon = True


class Visualizer (tk.Tk):

    def __init__(self, master, proxyBackEnd, topic):

        self.master = master
        #socket related variables
        self.endPoint = proxyBackEnd
        self.context = zmq.Context()


        #data related variables
        self.data = None# will be initiliazed at play button command
        self.dataInitialized = False  # will be used to check if the first message was already received, which means data container was initialized
        self.topic = topic

        #data thread related variables
        self.dataManagerThread = collectDataThread(self)#threading.Thread(target=self.collectData)#
        self.proxyThread = proxyThread(self)
        self.canPlay = threading.Event() #initiliazed to False
        self.playing = threading.Event()
        self.pausing = threading.Event()
        self.running = threading.Event()
        self.quiting = threading.Event()

        #Gui related variables
        self.controlFrame = tk.LabelFrame(self.master, text = 'Control')
        self.controlFrame.pack(side="top", expand=True , fill = 'both')
        self.playButton = tk.Button(self.controlFrame, text="Play", command=self.playCommand)
        self.playButton.pack(side="left", expand=True)


        self.pauseButton = tk.Button(self.controlFrame, text="Pause", command=self.pauseCommand)
        self.pauseButton.pack(side="left", expand=True)

        self.csvButton = tk.Button(self.controlFrame, text="Export to CSV", command=self.exportToCsv)
        self.csvButton.pack(side="left", expand=True)

        self.methodOptionsFrame = MethodOptionsFrame(self.master)

        self.methodOptionsFrame.pack(side="top", fill="both", expand=True)
        self.quitButton = tk.Button(self.master, text="Quit", command=self.quitCommand)
        self.quitButton.pack(side="top", expand=True)

        self.plottingFunction = None #will be set after pressing the play button, according to method options


        pyplot.ion() #enable interactive plotting
        self.running.set()
        self.visualize() #run visaulizer loop

    def runProxy(self):

        frontEnd = self.context.socket(zmq.XSUB)
        frontEnd.bind(FRONT_ENDPOINT)

        backEnd = self.context.socket(zmq.XPUB)
        backEnd.bind(BACK_ENDPOINT)

        try :
            zmq.proxy(frontEnd, backEnd)
        except zmq.ZMQError :
            frontEnd.close()
            backEnd.close()


    def collectData(self):

        vizSubsricber = self.context.socket(zmq.SUB)
        vizSubsricber.connect(self.endPoint)
        time.sleep(1)

        vizSubsricber.connect(BACK_ENDPOINT)
        time.sleep(1)
        vizSubsricber.setsockopt(zmq.SUBSCRIBE, "")


        while self.running.is_set() :

                message = vizSubsricber.recv()

                #parse message
                pairs = self.parseMessage(message)
                if pairs == 0 : #this case avoids blocking on above recv if message rate is low (more than few secondes)
                    break
                elif pairs == -1 : #case of the received topic was not requested
                    continue

                #store in DataContainer
                self.data.storeData(pairs)
                if ( self.dataInitialized == False ) : #the first message received - allow plotting
                    self.dataInitialized = True

        #if we arrive here quit was pressed
        vizSubsricber.close()



    def visualize(self):

        if self.dataInitialized and not( self.pausing.is_set() ) :

            self.data.dataReady.wait() #will wait iff data is being updated (if DataContainer.storeData() is running)
            self.plottingFunction()
        if self.running.is_set(): #tk GUI main loop

            self.master.after(1000, self.visualize)
        #if we arrive here - quit was pressed
        else :
            pyplot.close()
            self.dataManagerThread.join()
            self.context.destroy()
            self.proxyThread.join()
            self.master.destroy()




    def playCommand(self):


        if self.pausing.is_set() == False and self.playing.is_set() == False : #to handle play click at first time

            self.data = DataContainer()
            if self.methodOptionsFrame.methodVar.get() == XY :
                self.plottingFunction = self.plotXY
            else :
                self.plottingFunction = self.plotTimeDependent
                self.data.isTimeDependent = True

            self.methodOptionsFrame.disableOptions() #disable plot method change during plotting

            self.playing.set()

            time.sleep(1)  # allow time to connect
            #self.dataManagerThread.start() #start data collect thread
            self.proxyThread.start()
            self.dataManagerThread.start()  # start data collect thread
            time.sleep(1)  # allow time to init

        elif self.pausing.is_set() == True and self.playing.is_set() == False : #case pause is pressed
            self.pausing.clear()
            self.playing.set()



    def pauseCommand(self):
        if self.playing.is_set() == True and  self.pausing.is_set() == False: #if not running or already paused - do nothing
            self.playing.clear()
            self.pausing.set()

    def quitCommand(self):

        self.running.clear() #will signal visualize loop to close thread and quit
        
        nodeManager = NodeManager.NodeManager()
        shutDownMessage = nodeManager.xmlBuilder('terminate', ['*'], ['*'], 0)

        publisher = self.context.socket(zmq.PUB)
        publisher.connect(FRONT_ENDPOINT)
        time.sleep(1)
        publisher.send(shutDownMessage)#signal subscriber to close

        time.sleep(0.1)
        publisher.close()
        time.sleep(0.1)



    def plotXY(self):


        self.data.notPlotting.clear()

        xAxis = self.data.axesDataLists[1].getData()
        yAxis = self.data.axesDataLists[2].getData()

        pyplot.scatter(xAxis, yAxis, color='r')
        #set axes labels
        axes = pyplot.gca()
        axes.set_xlabel(self.data.axesDataLists[1].axisName)
        axes.set_ylabel(self.data.axesDataLists[2].axisName)

        self.data.notPlotting.set()
        # draw graph
        pyplot.draw()

    def plotTimeDependent(self):

        yMax = 0
        yMin = 0
        i = 0  # index variable

        self.data.notPlotting.clear()

        xAxis = self.data.axesDataLists[0].getData()

        namesForLegend = []
        valuesForLegend = []
        for ax in self.data.axesDataLists[1:len(self.data.axesDataLists)]:
            yAxis = ax.getData()
            namesForLegend .append( ax.axisName )
            yTempMax = max(yAxis)
            yTempMin = min(yAxis)

            # check for maximum values
            if yTempMax > yMax:
                yMax = yTempMax
            if yTempMin < yMin:
                yMin = yTempMin

            pathToLegend = pyplot.scatter(xAxis, yAxis, color=COLOURS[i])
            valuesForLegend.append(pathToLegend)

            i += 1


        # set axis limits - 25% above maximum value and below minimum value
        assert (yMax >= yMin)
        yRange = yMax - yMin
        axesAddition = yRange * 0.25
        axes = pyplot.gca()  # get control on axes
        axes.set_ylim([yMin - axesAddition, yMax + axesAddition])
        #set time axis label
        axes.set_xlabel('Time (sec)')
        #set legend
        legend = pyplot.legend(valuesForLegend, namesForLegend, loc = 'best', prop = FontProperties(), shadow=True)
        legend.draggable()
        #decreasse legend font size
        pyplot.setp(legend.get_title(), fontsize='xx-small')
        # draw graph
        pyplot.draw()

        self.data.notPlotting.set()



    """
    ###################################################################################
    Vizualizer.parseMessage function
    message - an Element Tree message
    Returns a list of pairs (fieldName, value). field name might be Timestamp.
    ###################################################################################
    """
    def parseMessage(self, message):


        root = ET.fromstring(message)
        topic = root.find("Header/Topic").text
        if int(topic) == 0 : #case of a message from service topic
            resultList = 0
        elif topic != self.topic : #case of message from topic that was not requsted by the user
            resultList = -1
        else :
            resultList = []
            timeStamp = root.find("Header/TimeStamp").text
            resultList.append((TS, timeStamp ) )
            body = root.find("Body")

            for child in body:
                if child.attrib.values()[0] == "real":
                    #append value
                    resultList.append( ( child.tag, float(child.text) ) )


        #print resultList #Debug
        return  resultList

    def exportToCsv(self):

        if not( self.playing.is_set() ) and self.pausing.is_set() and self.dataInitialized : #must be paused to export

            #merge data lists
            rowsList = []
            row = ['TimeStamp']
            for j in range(len(self.data.axesDataLists)):
                row.append(self.data.axesDataLists[j].axisName)
            rowsList.append(row)
            row = []
            for i in range( len(self.data.axesDataLists[0]) ) :
                row.append(self.data.timeStampList[i])
                for j in range( len(self.data.axesDataLists) ) :
                    row.append(self.data.axesDataLists[j].getData()[i])
                rowsList.append(row)
                row = []
            csvFile = open('visualizerCSVExport.csv', 'w')
            writer = csv.writer(csvFile)
            writer.writerows(rowsList)
            csvFile.close()

def center_window(root, width=250, height=200):
    # get screen width and height
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()

    # calculate position x and y coordinates
    x = (screen_width/2) - (width/2)
    y = (screen_height/2) - (height/2)
    root.geometry('%dx%d+%d+%d' % (width, height, x, y))

TITLE = 'NeMALA Visualizer'
def run(argv):

    if len(argv) < 3 :
        usage(argv[0])
    else :
        try :
            root = tk.Tk()
            root.title(TITLE)
            center_window(root)
            vis = Visualizer(root, argv[1], argv[2] ) #'ipc:///tmp/proxy_out.ipc', '6')
            root.mainloop()
        except KeyboardInterrupt :
            vis.quitCommand()

    return 0


"""
###################################################################################
# Run the main function
###################################################################################
"""
if __name__ == '__main__':
    sys.exit(run(sys.argv))
