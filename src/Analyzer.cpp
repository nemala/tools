/*
 * Analyzer.cpp
 *
 *  Created on: Jun 1, 2017
 *      Author: dave
 */

#include "Analyzer.h"
#include "AnalyzerMessage.h"
#include "Config.h"

namespace NeMALA_Tools
{

Analyzer::Analyzer(unsigned int unNodeId, std::string sSubscribeToSystemEndpoint,
		 std::string sPublishToEndpoint, unsigned int unTopic):
				 NeMALA::Dispatcher(NEMALA_TOOLS_VERSION_MAJOR,
						 NEMALA_TOOLS_VERSION_MINOR,
						 unNodeId,
						 sSubscribeToSystemEndpoint
						 ),
						m_Publisher(sPublishToEndpoint.c_str(), unTopic)
{
	AddPublisher(&m_Publisher);
}


//------------------------------------------------------------------------------------------------------------------------

void Analyzer::AddAccumulator(std::string strSampleName, Accumulator* pAccumulator)
{
	m_mAccumulators[strSampleName] = pAccumulator;
}

//-----------------------------------------------------------------------------------------------------------------------

void Analyzer::Accumulate(std::string strSampleName, double dVal)
{
	bool bReady(true);

	if(m_mAccumulators[strSampleName]->Accumulate(dVal))
	{
		// Check if all accumulators are saturated
		for (std::map<std::string,Accumulator*>::iterator it=m_mAccumulators.begin(); it!=m_mAccumulators.end(); ++it)
		{
			if(false == it->second->IsSaturated())
			{
				bReady = false;
				break;
			}
		}
		// If so, dump all of them into a message and publish
		if (bReady)
		{
			std::map<std::string, double> map;

			for (std::map<std::string,Accumulator*>::iterator it=m_mAccumulators.begin(); it!=m_mAccumulators.end(); ++it)
			{
				map[it->first] = it->second->Dump();
			}

			AnalyzerMessage analyzerMessage(map);
			m_Publisher.Publish(analyzerMessage);
		}
	}
}

} // namespace NeMALA_Tools
