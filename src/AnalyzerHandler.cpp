/*
 * AnalyzerHandler.h
 *
 *  Created on: May 29, 2016
 *      Author: dave
 */

#include "AnalyzerHandler.h"
#include <boost/foreach.hpp>

namespace NeMALA_Tools
{

//-----------------------------------------------------------------------------------------------------

void AnalyzerHandler::Handle(NeMALA::Proptree ptMsgBody)
{
	// For each field in m_smFields, get the "name" value from the message and accumulate it in the "rename"  accumulator.
	for (std::map<std::string,std::string>::iterator it=m_smFields.begin(); it!=m_smFields.end(); ++it)
	{
		m_pAnalyzer->Accumulate(it->second, ptMsgBody.get<double>(it->first));
	}
}

} // namespace NeMALA_Tools
