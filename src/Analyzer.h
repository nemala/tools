/*
 * Analyzer.h
 *
 *  Created on: May 29, 2017
 *      Author: dave
 */

#ifndef _ANALYZER_H_
#define _ANALYZER_H_

#include <NeMALA/Dispatcher.h>
#include "Accumulator.h"

namespace NeMALA_Tools
{

/*
 * The Analyzer is a tool used to bundle numeric fields from different topics,
 * for debugging or internal use by other nodes.
*/
class Analyzer : public NeMALA::Dispatcher
{

public:

	 /*
	  * Constructor
	  * @param unNodeId each node needs to have a node ID.
	  * @param sSubscribeToSystemEndpoint subscribe here to get system notifications such as pause, resume and terminate.
	  * @param sPublishToEndpoint publish to this end-point.
	  * @param unTopic publish to this topic.
	  */
	 Analyzer(unsigned int unNodeId, std::string sSubscribeToSystemEndpoint,
			 std::string sPublishToEndpoint, unsigned int unTopic);

	 virtual ~Analyzer(){}

	 /*
	  * Accumulate values
	  *  @param  strSampleName The name of the field being sampled.
	  *  @param  dVal The sample value
	  */
	 void Accumulate(std::string strSampleName, double dVal);

	 void AddAccumulator(std::string strSampleName, Accumulator* pAccumulator);

private:

	std::map<std::string,Accumulator*> m_mAccumulators;
	NeMALA::Publisher m_Publisher;
};

} // namespace NeMALA_Tools

#endif /* _ANALYZER_H_ */
