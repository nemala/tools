/*
 * AnalyzerMessage.cpp
 *
 *  Created on: Jun 1, 2017
 *      Author: dave
 */

#include "AnalyzerMessage.h"
#include <boost/foreach.hpp>

namespace NeMALA_Tools
{

AnalyzerMessage::AnalyzerMessage(std::map<std::string, double> mFields):
		m_mFeilds(mFields)
{
	BuildMessage();
}

//-------------------------------------------------------------------------------------------------

AnalyzerMessage::AnalyzerMessage(NeMALA::Proptree pt)
{
	BOOST_FOREACH(boost::property_tree::ptree::value_type& field, pt.get_child("Message.Body"))
	{
		m_mFeilds[field.first] = field.second.get_value<double>();
	}
	BuildMessage();
}

//-------------------------------------------------------------------------------------------------

void AnalyzerMessage::BuildMessage()
{
	// Check if all accumulators are saturated
	for (std::map<std::string,double>::iterator it=m_mFeilds.begin(); it!=m_mFeilds.end(); ++it)
	{
		AddElementNumber(it->first, it->second);
	}
}

}// namespace NeMALA_Tools
